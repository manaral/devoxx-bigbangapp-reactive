//
//  DetailViewController.m
//  BigBangApp
//
//  Created by Michael Seghers on 2/11/12.
//  Copyright (c) 2012 iDA MediaFoundry. All rights reserved.
//

#import "DetailViewController.h"
#import "AllDetailsTabBarController.h"
#import "Person.h"

@interface DetailViewController ()
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *professionLabel;
@property (strong, nonatomic) IBOutlet UILabel *realNameLabel;
@property (strong, nonatomic) IBOutlet UITextView *bioTextView;

- (void) configureView;

@end

@implementation DetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self configureView];
}

- (void)configureView {
    AllDetailsTabBarController *parent = (AllDetailsTabBarController *) self.parentViewController;
    Person *person = parent.person;
    self.nameLabel.text = person.fullName;
    self.professionLabel.text = person.profession;
    self.realNameLabel.text = person.realName;
    self.bioTextView.text = person.bio;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
