//
//  Person.h
//  BigBandApp
//
//  Created by Michael Seghers on 30/10/12.
//  Copyright (c) 2012 iDA MediaFoundry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject

@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *realName;
@property (nonatomic, strong) NSString *profession;
@property (nonatomic, strong) NSString *bio;
@property (nonatomic, strong) NSString *imageName;
@property (nonatomic, strong) NSString *website;
@property (nonatomic, readonly) NSString *fullName;

+ (Person *) personWithName:(NSString *) name andLastName:(NSString *) lastName;
+ (Person *) personWithContentsOfFile:(NSString *) path error:(NSError **) error;

- (id) initWithName:(NSString *) name andLastName:(NSString *) lastName;




@end
