//
//  Person.m
//  BigBandApp
//
//  Created by Michael Seghers on 30/10/12.
//  Copyright (c) 2012 iDA MediaFoundry. All rights reserved.
//

#import "Person.h"

@implementation Person

+ (Person *)personWithName:(NSString *)name andLastName:(NSString *)lastName {
    Person *person = [[Person alloc] initWithName:name andLastName:lastName];
    return person;
}

+ (Person *)personWithContentsOfFile:(NSString *)path error:(NSError *__autoreleasing *)error {
    NSDictionary *personDictionary = [NSDictionary dictionaryWithContentsOfFile:path];
    Person *person;
    if (personDictionary) {
        person = [Person personWithName:personDictionary[@"name"] andLastName:personDictionary[@"lastName"]];
        person.realName = personDictionary[@"realName"];
        person.profession = personDictionary[@"profession"];
        person.bio = personDictionary[@"bio"];
        person.imageName = personDictionary[@"imageName"];
        person.identifier = personDictionary[@"id"];
        person.website = personDictionary[@"website"];
    } else {
        if (error) {
            *error = [NSError errorWithDomain:@"PersonDomain" code:-100 userInfo:@{@"Reason" : @"File was not found"}];
        }
    }
    return person;
}

- (id)initWithName:(NSString *)name andLastName:(NSString *)lastName
{
    self = [super init];
    if (self) {
        self.name = name;
        self.lastName = lastName;
    }
    return self;
}

- (void)setLastName:(NSString *)lastName {
    _lastName = lastName;
    _fullName = [NSString stringWithFormat:@"%@ %@", _name, _lastName];
}

- (void)setName:(NSString *)name {
    _name = name;
    _fullName = [NSString stringWithFormat:@"%@ %@", _name, _lastName];
}

@end
