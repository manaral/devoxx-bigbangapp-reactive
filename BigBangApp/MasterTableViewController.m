//
//  MasterTableViewController.m
//  BigBangApp
//
//  Created by Michael Seghers on 2/11/12.
//  Copyright (c) 2012 iDA MediaFoundry. All rights reserved.
//

#import "MasterTableViewController.h"
#import "Person.h"
#import "NSString+Person.h"
#import "AllDetailsTabBarController.h"

@interface MasterTableViewController () {
    NSMutableArray *_persons;
}

- (void) addPersonFromFile:(NSString *)name;

@end

@implementation MasterTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    _persons = [[NSMutableArray alloc] init];
    [self addPersonFromFile:@"leonard"];
    [self addPersonFromFile:@"sheldon"];
    [self addPersonFromFile:@"penny"];
    [self addPersonFromFile:@"howard"];
    [self addPersonFromFile:@"rajesh"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 0;
}
*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return _persons.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"PersonCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    Person *person = _persons[indexPath.row];
    cell.textLabel.text = person.fullName;
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

#pragma mark - Private helper methods

- (void) addPersonFromFile:(NSString *) name {
    NSString *personPath = [[NSBundle mainBundle] pathForResource:name ofType:@"plist"];
    NSError *error;
    Person *p = [Person personWithContentsOfFile:personPath error:&error];
    if (p) {
        [_persons addObject:p];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Person error" message:[NSString stringWithFormat:@"Could not read person %@", name] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showDetail"]) {
        NSIndexPath *selectedRow = [self.tableView indexPathForSelectedRow];
        AllDetailsTabBarController *tabBarController = (AllDetailsTabBarController *) segue.destinationViewController;
        tabBarController.person = _persons[selectedRow.row];
    }
}

@end
