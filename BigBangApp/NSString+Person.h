//
//  NSString+Person.h
//  BigBangApp
//
//  Created by Michael Seghers on 2/11/12.
//  Copyright (c) 2012 iDA MediaFoundry. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Person;

@interface NSString (Person)

+ (NSString *) stringWithPerson:(Person *) person;

@end
