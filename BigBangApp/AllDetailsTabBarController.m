//
//  AllDetailsTabBarController.m
//  BigBangApp
//
//  Created by Michael Seghers on 2/11/12.
//  Copyright (c) 2012 iDA MediaFoundry. All rights reserved.
//

#import "AllDetailsTabBarController.h"
#import "WebsiteViewController.h"
#import "Person.h"

@interface AllDetailsTabBarController ()

- (IBAction)checkPersonUrl:(id)sender;
- (void) configureView;

@end

@implementation AllDetailsTabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self configureView];
}

- (IBAction)checkPersonUrl:(id)sender {
    if (self.person.website) {
        [self performSegueWithIdentifier:@"showWebsite" sender:sender];
    } else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Unable to open website" message:[NSString stringWithFormat:@"%@ has no website!", self.person.name] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
}

- (void)configureView {
    for (id controller in self.viewControllers) {
        if ([controller respondsToSelector:@selector(configureView)]) {
            [controller configureView];
        }
    }
    self.title = self.person.name;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showWebsite"]) {
        WebsiteViewController *websiteViewController = (WebsiteViewController *) segue.destinationViewController;
        websiteViewController.websiteUrl = [NSURL URLWithString:self.person.website];
    }
}

- (void)setPerson:(Person *)person {
    if (_person != person) {
        _person = person;
        
        // Update the view.
        [self configureView];
    }

}

@end
