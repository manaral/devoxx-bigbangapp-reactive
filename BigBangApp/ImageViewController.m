//
//  ImageViewController.m
//  BigBangApp
//
//  Created by Michael Seghers on 2/11/12.
//  Copyright (c) 2012 iDA MediaFoundry. All rights reserved.
//

#import "ImageViewController.h"
#import "AllDetailsTabBarController.h"
#import "Person.h"

@interface ImageViewController ()

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
- (void) configureView;

@end

@implementation ImageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self configureView];
}

- (void)configureView {
    AllDetailsTabBarController *parent = (AllDetailsTabBarController *) self.parentViewController;
    Person *person = parent.person;
    self.imageView.image = [UIImage imageNamed:person.imageName];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
