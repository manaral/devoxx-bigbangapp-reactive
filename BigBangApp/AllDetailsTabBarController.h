//
//  AllDetailsTabBarController.h
//  BigBangApp
//
//  Created by Michael Seghers on 2/11/12.
//  Copyright (c) 2012 iDA MediaFoundry. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Person;

@interface AllDetailsTabBarController : UITabBarController

@property (nonatomic, strong) Person *person;

@end
