//
//  NSString+Person.m
//  BigBangApp
//
//  Created by Michael Seghers on 2/11/12.
//  Copyright (c) 2012 iDA MediaFoundry. All rights reserved.
//

#import "NSString+Person.h"
#import "Person.h"

@implementation NSString (Person)

+ (NSString *)stringWithPerson:(Person *)person {
    return person.fullName;
}

@end
